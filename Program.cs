﻿using System;
using Microsoft.Extensions.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Reflection;
using Microsoft.Extensions.Configuration.UserSecrets;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace FreshsalesConnectionTest
{
    class Program
    {
        public static IConfigurationRoot Configuration;


        static async Task Main(string[] args)
        {

            try { 
                var builder = new ConfigurationBuilder()
               .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
               .AddJsonFile("appsettings.json", false)
               .AddUserSecrets(Assembly.GetExecutingAssembly());

                Configuration = builder.Build();

                var services = new ServiceCollection();

                ConfigureServices(services);


                using var serviceProvider = services.BuildServiceProvider();

                AccountFactory af = new(serviceProvider.GetRequiredService<FreshsalesService>());

                //Add new account
                Account newAccount = new() {Name="My new contact"};
                newAccount.CustomFields.Add("inactive", true);
                Account newCreatedAccoutn = await af.AddObject(newAccount);
                Console.WriteLine("");


                //ContactFactory cf = new(serviceProvider.GetRequiredService<FreshsalesService>());

                //double testID = 30013933079;

                //Get list of contacts
                //List<Contact> contacts = await cf.GetObjectList();
                //Console.WriteLine("");

                ////Get contact by ID
                //Contact result = await cf.GetObjectByID(testID);
                //Console.WriteLine("");

                ////Add new contact
                //Contact newContact = new() { FirstName = "Jan", LastName = "Janssen", JobTitle = "Manager", Email = "janssen@cmsnl.com" };
                //Contact newCreatedContact = await cf.AddObject(newContact);
                //Console.WriteLine("");


                ////Update contact

                //Contact contactToUpdate = new() { ID = testID, FirstName = "Henk", LastName = "van Dijk", Email = "jan@cmsnl.com" };
                //Contact updatedContact = await cf.UpdateObject(contactToUpdate, testID);
                //Console.WriteLine("");

                ////Remove contact
                //bool result = await cf.RemoveObjectByID(testID);
                //Console.Write(result.ToString());

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public static void ConfigureServices(ServiceCollection services)
        {
            services.AddHttpClient<FreshsalesService>();
        }
    }
}
