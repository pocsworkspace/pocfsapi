﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using FreshsalesConnectionTest.Helpers;

namespace FreshsalesConnectionTest
{
    public class Account:FreshSalesBaseClass
    {

        [JsonProperty("id")]
        public double ID { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
        
        

      
    }
}
