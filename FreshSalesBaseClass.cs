﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using FreshsalesConnectionTest.Helpers;

namespace FreshsalesConnectionTest
{
    public class FreshSalesBaseClass
    {
        [JsonConverter(typeof(CustomFieldConverter))]
        [JsonProperty("custom_field")]
        public Dictionary<string, object> CustomFields = new();
    }
}
