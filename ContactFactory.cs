﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreshsalesConnectionTest
{
    public  class ContactFactory : BaseFactory<Contact>
    {
        public override string objectURL => "contacts";

        public override double viewAllID => 30001425082;

        public ContactFactory(FreshsalesService freshsalesService)
        : base(freshsalesService)
        {
        }

      

        public async override Task<Contact> AddObject(Contact objectToAdd)
        {
            TransferObject result = await base.Post(objectToAdd);
            return result.Contact;
        }

        public async  override Task<Contact> GetObjectByID(double ID)
        {
            TransferObject result = await base.GetID(ID);
            return result.Contact; 
        }

        public async override Task<List<Contact>> GetObjectList()
        {
            TransferObject result = await base.GetList();
            return result.Contacts;
        }

        public async override Task<bool> RemoveObjectByID(double ID)
        {
            bool result = await base.Delete(ID);
            return result;
        }

        public async override Task<Contact> UpdateObject(Contact objectToAdd, double ID)
        {
            TransferObject result = await base.Put(objectToAdd, ID);
            return result.Contact;
        }
    }
}
