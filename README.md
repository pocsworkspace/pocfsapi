## Before you start
---


**Set your API key**

Don't forget to set your private API key as a value of the FreshsalesToken variable in appsettings.json (or your secret.json which is also supported by the project).

Use [this linkt](https://crmsupport.freshworks.com/support/solutions/articles/50000002503-how-to-find-my-api-key-) to find your API key in Freshsales.

---

**Set the correct IDs**

Your test IDs will be different. Don't forget to change 

 - the double testID variable 
 
 and 
 
 - the view ID by initializing the ContactFactory (put in the constructor as the last parameter)
  ContactFactory cf = new ContactFactory(serviceProvider.GetRequiredService<FreshsalesService>(), "contacts", **XXXXXXXXX**);
 
 You can obtain the new values from the webinterface, by clicking on a contact/view en retrieving the IDs from the URL.
 
