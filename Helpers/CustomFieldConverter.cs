﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FreshsalesConnectionTest.Helpers
{
    public class CustomFieldConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {

            Dictionary<string, object> result = new Dictionary<string, object>();

            Dictionary<string, object> deserialized = serializer.Deserialize<Dictionary<string, object>>(reader);

            foreach(KeyValuePair<string,object> pair in deserialized)
            {
                result.Add(pair.Key.Replace("cf_", ""), pair.Value);
            }

            return result;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var customFields = value as Dictionary<string, object>;
            Dictionary<string, object> fieldsToSerialize = new();

            foreach (KeyValuePair<string, object> pair in customFields)
            {
                fieldsToSerialize.Add("cf_" + pair.Key, pair.Value);
            }

            JToken t = JToken.FromObject(fieldsToSerialize);
          
            JObject o = (JObject)t;

            o.WriteTo(writer);
          
        }
    }
}
