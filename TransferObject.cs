﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FreshsalesConnectionTest
{
    [Serializable]
    public class TransferObject
    {
        [JsonProperty("contact")]
        public Contact Contact { get; set; }


        [JsonProperty("contacts")]
        public List<Contact> Contacts { get; set; }

        [JsonProperty("sales_account")]
        public Account Account { get; set; }

        [JsonProperty("sales_accounts")]
        public List<Account> Accounts { get; set; }
    }
}
