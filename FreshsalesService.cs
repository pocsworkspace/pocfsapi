﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FreshsalesConnectionTest
{
    public class FreshsalesService
    {
		private const string APIToken = "bkEBUd9NAZ6AV2197xU3xA";
		public HttpClient Client { get; }

		public FreshsalesService(HttpClient client)
		{
			client.BaseAddress = new Uri("https://cmsnl-team.myfreshworks.com/crm/sales/");
			client.DefaultRequestHeaders.Add("Authorization", "Token token=" + Program.Configuration.GetSection("FreshsalesToken").Value);
			client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
			
			Client = client;
		}

		/// <summary>
		/// Fetch an object by ID
		/// </summary>
		/// <param name="id">The unique object ID</param>
		/// <param name="objectURL">Part of the URL for the chosen type of object</param>
		/// <returns></returns>
		public async Task<TransferObject> GetID( string objectURL, double id)
		{
			string url = "api/" + objectURL + "/" + id.ToString();
			return await Get(url);

		}

		/// <summary>
		/// Gets the result from the url
		/// </summary>
		/// <param name="url">URL to get the data from</param>
		/// <returns></returns>
		private async Task<TransferObject> Get(string url)
		{
			HttpResponseMessage response = await this.Client.GetAsync(url);

			return await ReadFromResponse(response);
		}

		/// <summary>
		/// Fetch a list of objects
		/// </summary>
		/// <param name="objectURL">Part of the URL for the chosen type of object</param>
		/// <returns></returns>
		public async Task<TransferObject> GetList(string objectURL, double viewAllID)
		{
			string url = "api/" + objectURL + "/view/" + viewAllID.ToString();
			return await Get(url);

		}

        /// <summary>
        /// Create an object
        /// </summary>
        /// <param name="objectURL">Part of the URL for the chosen type of object</param>
        /// <returns></returns>
        public async Task<TransferObject> Post(string objectURL, TransferObject transferObject)
        {
			var payload = EncodeObject(transferObject);

			HttpResponseMessage response = await this.Client.PostAsync("api/" + objectURL, payload);

			return await ReadFromResponse(response);
		}

		private StringContent EncodeObject(TransferObject transferObject)
		{
			string jsonString = JsonConvert.SerializeObject(transferObject);
			return new StringContent(jsonString, Encoding.UTF8, "application/json");

		}
		private  async Task<TransferObject> ReadFromResponse(HttpResponseMessage response)
		{
			string result = await response.Content.ReadAsStringAsync();

			if (response.IsSuccessStatusCode)
			{
				
				return JsonConvert.DeserializeObject<TransferObject>(result);
			}
			else
            {
				throw new Exception(result);
			}
		}

		/// <summary>
		/// Update an object
		/// </summary>
		/// <param name="objectURL">Part of the URL for the chosen type of object</param>
		/// /// <param name="id">The unique ID of the object to update</param>
		/// <returns></returns>
		public async Task<TransferObject> Put(string objectURL, double id, TransferObject transferObject)
        {
			var payload = EncodeObject(transferObject);

			string url = "api/" + objectURL + "/" + id.ToString();

			HttpResponseMessage response = await this.Client.PutAsync(url, payload);

			return await ReadFromResponse(response);

		}


		/// <summary>
		/// Remove an object
		/// </summary>
		/// <param name="objectURL">Part of the URL for the chosen type of object</param>
		/// <param name="id">The unique ID of the object to delete</param>
		/// <returns></returns>
		public async Task<bool> Delete(string objectURL, double id)
        {
			string url = "api/" + objectURL + "/" + id.ToString();

			HttpResponseMessage response = await this.Client.DeleteAsync(url);

			string result = await response.Content.ReadAsStringAsync();

			if (response.IsSuccessStatusCode)
			{
				return JsonConvert.DeserializeObject<bool>(result);
			}
			else
			{
				throw new Exception(result);
			}
		}

    }
}
