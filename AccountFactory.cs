﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreshsalesConnectionTest
{
    public class AccountFactory : BaseFactory<Account>
    {
        public override string objectURL { get => "sales_accounts"; }
        public override double viewAllID { get => 30001425105; }

        public AccountFactory(FreshsalesService freshsalesService)
        : base(freshsalesService)
        {
        }

      
        public async override Task<Account> AddObject(Account objectToAdd)
        {
            TransferObject result = await base.Post(objectToAdd);
            return result.Account;
        }

        public async override Task<Account> GetObjectByID(double ID)
        {
            TransferObject result = await base.GetID(ID);
            return result.Account;
        }

        public async  override Task<List<Account>> GetObjectList()
        {
            TransferObject result = await base.GetList();
            return result.Accounts;
        }

        public async override Task<bool> RemoveObjectByID(double ID)
        {
            bool result = await base.Delete(ID);
            return result;
        }

        public async override Task<Account> UpdateObject(Account objectToAdd, double ID)
        {
            TransferObject result = await base.Put(objectToAdd, ID);
            return result.Account;
        }
    }
}
