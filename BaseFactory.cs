﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Reflection;

namespace FreshsalesConnectionTest
{
    public  abstract class BaseFactory<T>
    {
		private readonly FreshsalesService _freshsalesService;
		public abstract string objectURL { get;  }
        public abstract double viewAllID { get; }

		public BaseFactory(FreshsalesService freshsalesService)
        {
			_freshsalesService= freshsalesService;
		}

        /// <summary>
        /// Fetch an object by ID
        /// </summary>
        /// <param name="id">The unique object ID</param>
        /// <returns></returns>
        public abstract Task<T> GetObjectByID(double ID);
        
        
        /// <summary>
        /// Fetch a list of objects
        /// </summary>
        /// <returns></returns>
        public abstract Task<List<T>> GetObjectList();

        /// <summary>
        /// Create an object
        /// </summary>
        /// <param name="objectToAdd"></param>
        /// <returns></returns>
        public abstract Task<T> AddObject(T objectToAdd);
        
        /// <summary>
        /// Update an object
        /// </summary>
        /// <param name="objectToAdd">Object to update</param>
        /// <param name="ID">The unique ID of the object to update</param>
        /// <returns></returns>
        public abstract Task<T> UpdateObject(T objectToAdd, double ID);
        
        /// <summary>
        ///Remove an object by ID 
        /// </summary>
        /// <param name="ID">The unique ID of the object to delete</param>
        /// <returns></returns>
        public abstract Task<bool> RemoveObjectByID(double ID);


        protected async Task<TransferObject> GetID(double ID)
        {
            return await _freshsalesService.GetID(this.objectURL, ID);
        }

        protected async Task<TransferObject> GetList()
        {
            return await _freshsalesService.GetList(this.objectURL, this.viewAllID);
        }

        protected async Task<bool> Delete(double ID)
        {
            return await _freshsalesService.Delete(this.objectURL, ID);
        }

        protected async Task<TransferObject> Post(T objectToAdd)
        {
            TransferObject transferObject = CreateTransferObjectFromObject(objectToAdd);

            return await _freshsalesService.Post(this.objectURL, transferObject);
        }
     
        protected async Task<TransferObject> Put(T objectToAdd, double ID)
        {
            TransferObject transferObject = CreateTransferObjectFromObject(objectToAdd);

            return await _freshsalesService.Put(this.objectURL, ID, transferObject);
        }

        protected TransferObject CreateTransferObjectFromObject(T objectToAdd)
        {
            TransferObject transferObject = new ();

            PropertyInfo prop = transferObject.GetType().GetProperty(objectToAdd.GetType().Name, BindingFlags.Public | BindingFlags.Instance);
            if (null != prop && prop.CanWrite)
            {
                prop.SetValue(transferObject, objectToAdd, null);
            }
            return transferObject;
        }
    }
}
